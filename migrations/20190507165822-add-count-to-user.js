"use strict"

module.exports = {
	up: (queryInterface, Sequelize) =>
		queryInterface.addColumn("users", "count", {
			type: Sequelize.INTEGER,
			defaultValue: 0
		}),
	down: queryInterface => queryInterface.removeColumn("users", "count")
}
