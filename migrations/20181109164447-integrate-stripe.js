module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.sequelize.transaction(transaction => {
			return Promise.all([
				queryInterface.removeColumn("plans", "creditsPerMonth", { transaction }),

				queryInterface.addColumn("companies", "stripeCustomerId", Sequelize.STRING, { transaction }),

				queryInterface.addColumn(
					"plans",
					"stripeId",
					{
						type: Sequelize.STRING
					},
					{ transaction }
				),

				queryInterface.addColumn(
					"plans",
					"billingInterval",
					{
						type: Sequelize.STRING,
						defaultValue: "month"
					},
					{ transaction }
				),

				queryInterface.addColumn(
					"plans",
					"accessLevel",
					{
						type: Sequelize.STRING,
						defaultValue: "low"
					},
					{ transaction }
				)
			])
		})
	},

	down: function(queryInterface) {
		return queryInterface.sequelize.transaction(transaction => {
			return Promise.all([
				queryInterface.removeColumn("companies", "stripeCustomerId", { transaction }),
				queryInterface.removeColumn("plans", "stripeId", { transaction }),
				queryInterface.removeColumn("plans", "billingLevel", { transaction }),
				queryInterface.removeColumn("plans", "accessLevel", { transaction })
			])
		})
	}
}
