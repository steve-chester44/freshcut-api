"use strict"
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable("BlockedTimes", {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			employeeId: {
				type: Sequelize.INTEGER,
				references: {
					model: "users",
					key: "id"
				}
			},
			locationId: {
				type: Sequelize.INTEGER,
				references: {
					model: "locations",
					key: "id"
				}
			},
			description: Sequelize.STRING,
			endTime: {
				type: Sequelize.DATE,
				allowNull: false,
				validate: {
					notEmpty: {
						msg: "An end time is required for blocked times."
					}
				}
			},
			startTime: {
				type: Sequelize.DATE,
				allowNull: false,
				validate: {
					notEmpty: {
						msg: "A start time is required for blocked times."
					}
				}
			},
			createdAt: Sequelize.DATE,
			updatedAt: Sequelize.DATE
		})
	},
	down: queryInterface => queryInterface.dropTable("BlockedTimes")
}
