import config from "../config"
import twilio from "twilio"

const client = new twilio(config.twilio.accountSid, config.twilio.authToken)

export const sendText = (to, body) => {
	client.messages.create({
		to,
		body,
		from: config.twilio.phoneNumber
	})
}

export const validateNumber = number => client.lookups.phoneNumbers(number).fetch()
