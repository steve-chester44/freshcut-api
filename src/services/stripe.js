import config from "../config"
import Stripe from "stripe"

export const stripe = Stripe(config.STRIPE_SECRET)