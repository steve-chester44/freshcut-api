import { okErrorResponse, okSuccessResponse } from "../../utils"
import { NotFoundError } from "../../utils/errors"

export default async function({ input }, ctx) {
	try {
		const user = await ctx.models.User.findOne({ where: { resetPasswordToken: input.token }, raw: true })

		if (!user) {
			throw new NotFoundError()
		}

		await ctx.models.User.update(
			{ password: input.password, resetPasswordToken: null },
			{
				omitNull: false,
				where: {
					id: user.id
				}
			}
		)

		return okSuccessResponse({
			user: {
				email: user.email
			}
		})
	} catch (error) {
		return okErrorResponse({ errors: [error] })
	}
}
