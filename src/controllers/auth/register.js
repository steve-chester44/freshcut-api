import { okErrorResponse, okSuccessResponse } from "../../utils"
import { sendEmail, ACCOUNT_SIGN_UP } from "../../utils/email"
import setYear from "date-fns/set_year"
import { stripe } from "../../services/stripe"

export default async ({ email, password, companyName = "Default Company", firstName, lastName }, models) => {
	const name = companyName.length === 0 ? "Default Company" : companyName

	try {
		const response = await models.sequelize.transaction(async transaction => {
			const company = await models.Company.create({ name }, { transaction })

			const location = await models.Location.create(
				{ name: "Default Location", address: "Default Address", companyId: company.id },
				{ transaction }
			)

			const user = await company.createUser(
				{ email, password, firstName, lastName, permissionLevel: "owner" },
				{ transaction }
			)

			const employee = await models.Employee.create({ userId: user.id, locationId: location.id }, { transaction })

			const group = await models.ServiceGroup.create({ name: "Default Group", companyId: company.id }, { transaction })

			await group.createService(
				{ name: "Default Service", companyId: company.id, duration: 15, price: 0 },
				{ transaction }
			)

			const plan = await models.Plan.findOne({ where: { accessLevel: "free" } })

			const customer = await stripe.customers.create({
				email: email,
				plan: plan.stripeId
			})

			await models.Subscription.create(
				{
					companyId: company.id,
					planId: plan.id,
					start: new Date(),
					end: setYear(new Date(), 3000)
				},
				{ transaction }
			)

			await company.update({ stripeCustomerId: customer.id }, { transaction })

			sendEmail(email, ACCOUNT_SIGN_UP, { companyName: name })

			return { company, user, location, employee }
		})

		return okSuccessResponse(response)
	} catch (error) {
		console.log(error)
		return okErrorResponse({ errors: error.errors })
	}
}
