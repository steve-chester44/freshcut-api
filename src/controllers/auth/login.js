import bcrypt from "bcrypt"
import { okSuccessResponse, okErrorResponse } from "../../utils"
import { createAndSetTokens } from "../../auth/create-tokens"

export default async (email, password, models, res) => {
	const user = await models.User.findOne({ where: { email } })

	if (!user) {
		return okErrorResponse({ errors: [{ path: "default", message: "Invalid email and/or password" }] })
	}

	if (!user.password) {
		return okErrorResponse({
			errors: [
				{
					path: "auth",
					message: "Account does not have a password set. Please use the forgotten password link."
				}
			]
		})
	}

	const valid = await bcrypt.compare(password, user.password)

	if (!valid) {
		return okErrorResponse({ errors: [{ path: "default", message: "Invalid email and/or password" }] })
	}

	const { accessToken } = createAndSetTokens(user, res)

	return okSuccessResponse({ token: accessToken })
}
