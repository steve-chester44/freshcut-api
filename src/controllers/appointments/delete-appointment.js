import { ForbiddenError } from "../../utils/errors"
import { isAuthenticatedResolver } from "../../auth/permissions"
import { pubsub } from "../../graphql"
import { subscriptions } from "../../graphql/constants"

export default isAuthenticatedResolver.createResolver(async (root, { id }, ctx) => {
	const appointment = await ctx.models.Appointment.findByPk(id)

	if (!appointment) throw new NotFoundError()

	const location = await ctx.models.Location.findByPk(appointment.locationId)

	if (!location || location.companyId !== ctx.user.companyId) {
		throw new ForbiddenError()
	}

	appointment.dataValues.status = "deleted"

	// TODO: Not sure if we actually want to do this but... if we delete an appointment then reduce the customers booking count
	if (appointment.customerId) {
		const customer = await ctx.models.Customer.findByPk(appointment.customerId)

		if (customer) {
			customer.update({ totalBookings: customer.totalBookings - 1 })
		}
	}

	pubsub.publish(subscriptions.APPOINTMENT_UPDATED, {
		AppointmentsChange: {
			employeeId: appointment.dataValues.userId,
			appointment: appointment.dataValues
		}
	})

	// TODO: The following shouldn't be needed if we set up our associations correctly.
	// Deleting an Appointmnet should cause a Sale to be deleted which deletes any payments.
	// Deleting this Sale should delete any appointments related to it.
	// ctx.models.Sale.destroy({ where: { appointmentId: id } })

	return await appointment.destroy()
})
