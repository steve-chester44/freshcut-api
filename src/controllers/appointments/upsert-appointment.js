import { isAuthenticatedResolver } from "../../auth/permissions"
import { ForbiddenError } from "../../utils/errors"

import { okErrorResponse, okSuccessResponse, getInputLocation } from "../../utils"

import { pubsub } from "../../graphql"
import { subscriptions } from "../../graphql/constants"
import { differenceInMinutes, subMinutes, format, parse } from "date-fns"

export default isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
	try {
		input.locationId = getInputLocation(ctx.user, input)

		if (!input.locationId) {
			throw new ForbiddenError()
		}

		const employee = await ctx.models.Employee.findByPk(input.userId)

		if (employee.locationId !== input.locationId) {
			throw new ForbiddenError()
		}

		const response = await ctx.models.sequelize.transaction(async transaction => {
			const appointmentData = { ...input }

			let appointment

			const customer = await ctx.models.Customer.findByPk(input.customerId)

			const hasServices = Array.isArray(input.services)

			if (hasServices) {
				const services = await ctx.models.Service.findAll({
					where: {
						id: {
							[ctx.models.Sequelize.Op.or]: input.services
						}
					},
					raw: true
				})

				// Set price and duration based on prices.
				if (services) {
					let price = 0
					let duration = 0

					for (let i = 0; i < services.length; i++) {
						if (!appointmentData.price) {
							price = services[i].price + price
						}

						if (!appointmentData.duration) {
							duration = services[i].duration + duration
						}
					}

					if (!appointmentData.price) {
						appointmentData.price = price
					}

					if (!appointmentData.duration) {
						appointmentData.duration = duration
					}
				}
			}

			if (customer) {
				appointmentData.customerId = customer.id
			} else {
				appointmentData.customerId = null
			}

			if (input.id) {
				const entity = await ctx.models.Appointment.findByPk(input.id)

				if (!entity) {
					throw new NotFoundError()
				}

				// Dock the old customer a totalBooking count if we have a new employee, else give the new employee a totalBooking count, if we're adding a customer
				if (entity.dataValues.customerId) {
					if (!customer || entity.dataValues.customerId !== customer.dataValues.id) {
						const oldCustomer = await ctx.models.Customer.findByPk(entity.dataValues.customerId)

						if (oldCustomer) {
							await oldCustomer.update(
								{ totalBookings: oldCustomer.dataValues.totalBookings - 1 },
								{ transaction }
							)

							if (customer) {
								await customer.update(
									{ totalBookings: customer.dataValues.totalBookings + 1 },
									{ transaction }
								)
							}
						}
					}
				} else {
					if (customer) {
						await customer.update({ totalBookings: customer.dataValues.totalBookings + 1 }, { transaction })
					}
				}

				appointment = await entity.update(appointmentData, { transaction })
			} else {
				appointment = await ctx.models.Appointment.create(appointmentData, { transaction })

				if (customer) {
					customer.update({ totalBookings: customer.totalBookings + 1 })
				}
			}

			if (hasServices) {
				await appointment.setServices(input.services, { transaction })
			}

			// TODO: Why is appointment.isNewRecord always false??
			const payload = {
				AppointmentsChange: {
					employeeId: appointment.dataValues.userId,
					appointment: appointment.dataValues
				}
			}

			pubsub.publish(
				appointment.isNewRecord ? subscriptions.APPOINTMENT_CREATED : subscriptions.APPOINTMENT_UPDATED,
				payload
			)

			return { appointment: appointment.dataValues, customer }
		})

		// only send the text message if the appointment is at least 20 minutes from now.
		if (response.customer) {
			const minutesUntilAppt = differenceInMinutes(parse(response.appointment.startTime), new Date())

			if (minutesUntilAppt > 20) {
				ctx.models.Location.findByPk(input.locationId).then(async location => {
					const company = await location.getCompany()
					const time = format(subMinutes(response.appointment.startTime, 15), "h:mmA")
					const date = format(response.appointment.startTime, "M/D")

					response.customer.notifyBySMS(
						`${response.customer.firstName}, This is to confirm your appointment at ${
							company.name
						}. Please arrive for your appointment on ${date} by no later than ${time}.`
					)
				})
			}
		}

		return okSuccessResponse({ appointment: response.appointment })
	} catch (error) {
		return okErrorResponse({ errors: [error] })
	}
})
