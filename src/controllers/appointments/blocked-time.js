export default (ctx, query = {}, input = {}) => {
	if (input.limit) {
		query.limit = parseInt(input.limit, 10)
	}

	if (input.order) {
		query.order = input.order
	}

	if (input.where) {
		query.where = { ...query.where, ...input.where }
	}

	return ctx.models.BlockedTime.findAll(query)
}
