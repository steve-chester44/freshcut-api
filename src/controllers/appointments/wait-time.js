import isBefore from "date-fns/is_before"
import differenceInMinutes from "date-fns/difference_in_minutes"
import isAfter from "date-fns/is_after"
import addMinutes from "date-fns/add_minutes"

import models from "../../models"

export const getWaitTime = async ({ locationId, employeeId, models }) => {
	const employee = await models.Employee.findOne({
		where: {
			locationId,
			userId: employeeId
		}
	})

	if (!employee) return { error: "Employee not found" }

	const appointments = await models.Appointment.findAll({
		raw: true,
		where: {
			locationId,
			userId: employeeId,
			status: {
				[models.Sequelize.Op.or]: {
					[models.Sequelize.Op.not]: "deleted",
					[models.Sequelize.Op.not]: "completed"
				}
			}
		}
	})

	const blockedTimes = await models.BlockedTime.findAll({ raw: true, where: { employeeId, locationId } })

	let index = undefined
	const now = new Date()

	// sort by startTime so appointments are in the order of which they occur
	const sortedAppointments = [...appointments, ...blockedTimes]
		.filter(({ endTime }) => isAfter(endTime, now))
		.sort((a, b) => new Date(a.startTime) - new Date(b.startTime))

	for (let i = 0; i < sortedAppointments.length; i++) {
		const current = sortedAppointments[i]

		// if the first event is more than 20 minutes away then break because theres room for an appointment.
		if (i === 0 && isBefore(addMinutes(now, 20), current.startTime)) {
			break
		}

		const difference = differenceInMinutes(
			current.startTime,
			sortedAppointments[i - 1] ? sortedAppointments[i - 1].endTime : now
		)

		// if the difference between the current appointment is more than 20 minutes from the last appointment, then set the last appointment as the last appointment. else set the current appointment as the last appointment.
		if (difference > 20) {
			index = i - 1
			break
		} else {
			index = i
		}
	}

	return { time: isNaN(index) ? 0 : differenceInMinutes(sortedAppointments[index].endTime, now) }
}

export default async (req, res) => {
	const { key, employeeId } = req.query

	if (!key) {
		return res.send({
			error: "API key is required"
		})
	}

	if (!employeeId) {
		return res.send({
			error: "Employee ID is required"
		})
	}

	const token = await models.AccessToken.findOne({ where: { key }, raw: true })

	if (!token) {
		return res.send({ error: "Invalid token" })
	}

	getWaitTime({ locationId: token.locationId, employeeId, models }).then(time => {
		res.send(time)
	})
}
