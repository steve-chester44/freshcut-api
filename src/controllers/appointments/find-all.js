export default (ctx, query = {}, input = {}) => {
	if (input.limit) {
		query.limit = parseInt(input.limit, 10)
	}

	if (input.order) {
		query.order = input.order
	}

	if (input.upcoming) {
		query.where = query.where || {}
		query.limit = parseInt(input.upcoming, 10)
		query.where.startTime = { [ctx.models.Sequelize.Op.gte]: new Date() }
	}

	if (input.past) {
		query.where = query.where || {}
		query.limit = parseInt(input.past, 10)
		query.where.endTime = { [ctx.models.Sequelize.Op.lte]: new Date() }
	}

	if (input.where) {
		query.where = { ...query.where, ...input.where }
	}

	return ctx.models.Appointment.findAll(query)
}
