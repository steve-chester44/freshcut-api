import crypto from "crypto"
import { okErrorResponse, okSuccessResponse } from "../../utils"
import { NotFoundError } from "../../utils/errors"
import { sendEmail, CONFIRM_ACCOUNT, ACCOUNT_ENABLED, ACCOUNT_DISABLED } from "../../utils/email"

export default async ({ input }, ctx) => {
	try {
		let employee
		let created = false

		const data = {
			...input,
			companyId: ctx.user.companyId,
			email: input.email && input.email.length >= 1 ? input.email : null
		}

		if (input.id) {
			employee = await ctx.models.User.findOne({
				where: { id: input.id, companyId: ctx.user.companyId }
			})

			if (!employee) throw new NotFoundError()

			const permissionLevel = employee.dataValues.permissionLevel

			await employee.update(data)

			// send an account disabled email if the account has been disabled
			if (permissionLevel !== data.permissionLevel && data.permissionLevel === "none") {
				sendEmail(data.email || employee.dataValues.email, ACCOUNT_DISABLED, {
					firstName: data.firstName || employee.dataValues.firstName
				})
			}
			console.log(permissionLevel, data.permissionLevel)
			
			// send either a confirm-account email or a 'your account has been enabled' type password
			if (permissionLevel !== data.permissionLevel && permissionLevel === "none") {
				if (!employee.password || employee.dataValues.password.length === 0) {
					const resetPasswordToken = crypto.randomBytes(20).toString("hex")
					employee.update({ resetPasswordToken })

					ctx.models.Company.findByPk(ctx.user.companyId, { raw: true }).then(company => {
						sendEmail(data.email, CONFIRM_ACCOUNT, {
							firstName: data.firstName || employee.firstName,
							company: company.name,
							url: `http://${ctx.req.headers.host}/confirm-account/${resetPasswordToken}`
						})
					})
				} else {
					sendEmail(data.email || employee.dataValues.email, ACCOUNT_ENABLED, {
						firstName: data.firstName || employee.dataValues.firstName
					})
				}
			}
		} else {
			const createToken = data.permissionLevel !== "none"

			if (createToken) {
				data.resetPasswordToken = crypto.randomBytes(20).toString("hex")
			}

			employee = await ctx.models.User.create(data)
			created = true

			if (createToken) {
				const company = await ctx.models.Company.findByPk(ctx.user.companyId, { raw: true })

				sendEmail(data.email, CONFIRM_ACCOUNT, {
					firstName: data.firstName,
					company: company.name,
					url: `http://${ctx.req.headers.host}/confirm-account/${data.resetPasswordToken}`
				})
			}
		}

		if (input.locations) {
			await employee.setLocations(input.locations.length > 0 ? input.locations : null)
		}

		if (input.services) {
			await employee.setServices(input.services.length > 0 ? input.services : null)
		}

		return okSuccessResponse({ employee, created })
	} catch (error) {
		console.log(error)
		return okErrorResponse({ errors: error.errors })
	}
}
