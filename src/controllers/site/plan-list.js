require("dotenv").config()

const planList = [
	{
		accessLevel: "free",
		name: "Free",
		month: {
			id: process.env.STRIPE_PLAN_FREE,
			price: 0
		},
		year: {
			id: process.env.STRIPE_PLAN_FREE,
			price: 0
		},
		features: ["1 Employee", "Unlimited Appointments", "Email Support"]
	},
	{
		accessLevel: "low",
		name: "Basic",
		month: {
			id: process.env.STRIPE_PLAN_MONTH_LOW,
			price: 29
		},
		year: {
			id: process.env.STRIPE_PLAN_YEAR_LOW,
			price: 299
		},
		features: ["3 Employees", "Unlimited Appointments", "Wait Time Widget", "Email Support"]
	},
	{
		name: "Fresh",
		accessLevel: "medium",
		month: {
			id: process.env.STRIPE_PLAN_MONTH_MED,
			price: 79
		},
		year: {
			id: process.env.STRIPE_PLAN_YEAR_MED,
			price: 799
		},

		features: [
			"10 Employees",
			"Unlimited Appointments",
			"Wait Time Widget",
			"Check In App",
			"Professional Reports",
			"Email Support"
		]
	},
	{
		name: "Super Fresh",
		accessLevel: "high",
		month: {
			id: process.env.STRIPE_PLAN_MONTH_HIGH,

			price: 99
		},
		year: {
			id: process.env.STRIPE_PLAN_YEAR_high,

			price: 999
		},
		features: [
			"Unlimited Employees",
			"Unlimited Appointments",
			"Wait Time Widget",
			"Check In App",
			"Professional Reports",
			"Advanced Reports Builder",
			"Email & Phone Support"
		]
	}
]

export default (req, res) => {
	res.send(planList)
}
