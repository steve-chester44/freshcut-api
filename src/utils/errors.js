import { createError } from "apollo-errors"

export const UnknownError = createError("UnknownError", {
	message: "An unknown error has occurred!"
})

export const ForbiddenError = createError("ForbiddenError", {
	path: "auth",
	message: "This action is forbidden"
})

export const NotYourEntityError = createError("NotYourEntityError", {
	path: "auth",
	message: "You do not have permission to make this request."
})

export const AuthenticationRequiredError = createError("AuthenticationRequiredError", {
	path: "auth",
	message: "You must be logged in"
})

export const ExpiredTokenError = createError("ExpiredTokenError", {
	path: "auth",
	message: "Auth code is expired"
})

export const MissingInputError = createError("MissingInputError", {
	path: "form",
	message: "Required input is missing."
})

export const NotFoundError = createError("NotFoundError", {
	path: "resource",
	message: "The entity was not found"
})
