export const okSuccessResponse = payload => ({ ok: true, ...payload })

export const okErrorResponse = ({ errors = [], ...payload }) => ({
	ok: false,
	errors: errors.map(({ path, message }) => ({ path, message })),
	...payload
})

export const getUserCompanyId = user => user.companyId
export const isAPIKeyRequest = user => user && user.isAPIKey && user.key && user.locationId

export const getInputLocation = (user, input) => (isAPIKeyRequest(user) ? user.locationId : input.locationId)
