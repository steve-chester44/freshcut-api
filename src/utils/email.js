import sendgrid from "@sendgrid/mail"

import config from "../config"
sendgrid.setApiKey(config.email.apiKey)

export const ACCOUNT_SIGN_UP = "EMAIL/ACCOUNT/REGISTERED"
export const CONFIRM_ACCOUNT = "EMAIL/ACCOUNT/CONFIRM"
export const ACCOUNT_DELETED = "EMAIL/ACCOUNT/DELETED"
export const ACCOUNT_ENABLED = "EMAIL/ACCOUNT/PERMISSION_CHANGE"
export const ACCOUNT_DISABLED = "EMAIL/ACCOUNT/DISABLED"

const getTemplateId = template => {
	switch (template) {
		case ACCOUNT_ENABLED:
			return "d-9fda0b0ac57c49c9bbde75adae8b9ab8"
		case ACCOUNT_DISABLED:
			return "d-66b27c90039e41cab181ab629ef02fb7"
		case ACCOUNT_DELETED:
			return "d-1a49de8b4ca8445182c40d5b65a60a30"
		case ACCOUNT_SIGN_UP:
			return "d-3407b986625341c6aa0f74428b665bfc"
		case CONFIRM_ACCOUNT:
			return "d-8fdad93a6259481abbcdee819ca6e990"
		default:
			return false
	}
}

export const sendEmail = (to, type, dynamic_template_data) => {
	const templateId = getTemplateId(type)

	if (!templateId) {
		console.log(`[EMAIL FAILED]: No email for type: (${type})`)
		return false
	}

	const params = {
		to,
		from: config.email.from,
		templateId,
		dynamic_template_data
	}

	console.log(`[EMAIL]: ${type}`)

	sendgrid.send(params)
}
