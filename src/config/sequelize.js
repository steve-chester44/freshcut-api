require("dotenv").config()

module.exports = {
	development: {
		username: "root",
		password: "root",
		database: "freshcut",
		host: "127.0.0.1",
		logging: true,
		dialect: "mysql"
	},
	production: {
		username: process.env.DATABASE_USER,
		password: process.env.DATABASE_PASS,
		database: process.env.DATABASE_NAME,
		host: process.env.DATABASE_HOST,
		logging: true,
		dialect: "mysql"
	}
}
