require("dotenv").config()
export const isProd = process.env.NODE_ENV === "production"

const PROD_URLS = [
	"https://app.freshcut.xyz",
	"https://checkin.freshcut.xyz",
	"https://neverwait.app",
	"https://checkin.neverwait.app",
	"https://laeffects.com",
	"https://www.laeffects.com"
]
const DEV_URLS = [
	"http://10.0.0.240:3001",
	"http://localhost:3000",
	"http://localhost:3001",
	"http://localhost:3002",
	"http://localhost:3003"
]

export default {
	isProd,
	CORS_ORIGINS: isProd ? PROD_URLS : DEV_URLS,
	BASE_API_PREFIX: process.env.NODE_ENV === "production" ? "/" : "/api",
	SUBSCRIPTION_PORT: 3443,
	SUBSCRIPTION_ENDPOINT: `/subscriptions`,
	STRIPE_SECRET: process.env.STRIPE_SECRET,
	email: {
		from: "support@neverwait.app",
		apiKey: process.env.SENDGRID_API_KEY
	},
	database: {
		host: process.env.DATABASE_HOST,
		name: process.env.DATABASE_NAME,
		user: process.env.DATABASE_USER,
		pass: process.env.DATABASE_PASS
	},
	twilio: {
		accountSid: process.env.TWILIO_ACCOUNT_SID,
		authToken: process.env.TWILIO_AUTH_TOKEN,
		phoneNumber: process.env.TWILIO_PHONE_NUMBER
	},
	secrets: {
		accessToken: process.env.ACCESS_TOKEN_SECRET,
		refreshToken: process.env.REFRESH_TOKEN_SECRET
	}
}
