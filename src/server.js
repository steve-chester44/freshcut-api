import express from "express"
import { createServer } from "http"
import cors from "cors"
import bodyParser from "body-parser"
import cookieParser from "cookie-parser"
import responseTime from "response-time"
import chalk from "chalk"
import expressPlayground from "graphql-playground-middleware-express"
import { graphqlExpress } from "apollo-server-express"

import { graphqlExpressConfig, createSubscriptionServer } from "./graphql"
import config from "./config"
import models from "./models"

import unauthorizedRequests from "./auth/unauthorized-requests"
import verifyTokens from "./auth/verify-tokens"
import waitTimeController from "./controllers/appointments/wait-time"
import planListController from "./controllers/site/plan-list"

const app = express()

app.use(responseTime())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cookieParser())

app.use(cors({ origin: config.CORS_ORIGINS, credentials: true }))

app.use(verifyTokens)
app.use(unauthorizedRequests)

app.use("/plans", planListController)
app.use("/wait", waitTimeController)
app.use("/playground", expressPlayground({ endpoint: config.BASE_API_PREFIX }))
app.use(config.BASE_API_PREFIX, graphqlExpress(graphqlExpressConfig))

models.sequelize.sync({ force: false }).then(
	() => {
		const server = createServer(app)

		server.listen(config.SUBSCRIPTION_PORT, () => {
			console.log(
				chalk.inverse.green(
					`Apollo Server is now running on ${
						config.isProd
							? `https://api.freshcut.xyz:${config.SUBSCRIPTION_PORT}`
							: `http://localhost:${config.SUBSCRIPTION_PORT}`
					}`
				)
			)
			createSubscriptionServer({ server, path: config.SUBSCRIPTION_ENDPOINT })
		})
	},
	error => {
		console.error(chalk.red(error))
		console.error(chalk.inverse.red(`Failed to start the DB. Is the DB running?`))
	}
)
