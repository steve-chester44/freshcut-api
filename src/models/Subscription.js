export default (sequelize, DataTypes) => {
	const Subscription = sequelize.define("subscription", {
		start: DataTypes.DATE,
		end: DataTypes.DATE
	})

	return Subscription
}
