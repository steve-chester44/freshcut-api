export default (sequelize, DataTypes) => {
	const Company = sequelize.define("company", {
		name: DataTypes.STRING,
		stripeCustomerId: DataTypes.STRING
	})

	Company.associate = models => {
		Company.hasMany(models.Location, { onDelete: "CASCADE" })
		Company.hasMany(models.User)
		Company.hasMany(models.Service)
		Company.hasMany(models.ServiceGroup)
		Company.belongsToMany(models.Plan, { through: models.Subscription })
	}

	return Company
}
