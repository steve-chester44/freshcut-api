export default (sequelize, DataTypes) => {
	const PaymentMethod = sequelize.define("payment_methods", {
		name: {
			type: DataTypes.STRING,
			unique: { msg: "A payment method with that name already exists.", fields: ["name"] },
			allowNull: false,
			validate: { notEmpty: true }
		},
		custom: {
			type: DataTypes.BOOLEAN,
			defaultValue: false
		}
	})

	return PaymentMethod
}
