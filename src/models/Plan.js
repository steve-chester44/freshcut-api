export default (sequelize, DataTypes) => {
	const Plan = sequelize.define("plan", {
		name: DataTypes.STRING,
		stripeId: DataTypes.STRING,
		pricePerMonth: DataTypes.FLOAT,
		accessLevel: DataTypes.STRING
	})

	Plan.associate = models => {
		Plan.belongsToMany(models.Company, { through: models.Subscription })
	}

	return Plan
}
