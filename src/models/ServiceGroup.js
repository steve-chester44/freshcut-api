export default (sequelize, DataTypes) => {
	const ServiceGroup = sequelize.define("service_group", {
		name: DataTypes.STRING
	})

	ServiceGroup.associate = models => {
		ServiceGroup.belongsTo(models.Company)
		ServiceGroup.hasMany(models.Service)
	}

	return ServiceGroup
}
