export default (sequelize, DataTypes) => {
	const Service = sequelize.define("service", {
		name: DataTypes.STRING,
		duration: DataTypes.INTEGER,
		price: DataTypes.FLOAT
	})

	Service.associate = models => {
		Service.belongsTo(models.Company)
		Service.belongsTo(models.ServiceGroup, { onDelete: "CASCADE" })
		Service.belongsToMany(models.User, { through: models.EmployeeService })

		Service.belongsToMany(models.Appointment, { through: "appointment_services" })
	}

	return Service
}
