export default (sequelize, DataTypes) => {
	const Location = sequelize.define("location", {
		name: {
			type: DataTypes.STRING,
			allowNull: false
		},
		address: DataTypes.STRING,
		contactEmail: { type: DataTypes.STRING, validate: { isEmail: { args: true, msg: "Invalid email" } } },
		contactNumber: DataTypes.STRING
	})

	Location.associate = models => {
		Location.belongsTo(models.Company)

		Location.belongsToMany(models.User, {
			through: models.Employee
		})

		Location.hasMany(models.BlockedTime)
	}

	return Location
}
