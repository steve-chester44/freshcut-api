export default (sequelize, DataTypes) => {
    const Payment = sequelize.define("payment", {
        amount: DataTypes.FLOAT,
        status: DataTypes.ENUM(['pending', 'completed'])
    })

    Payment.associate = (models) => {
        Payment.belongsTo(models.Sale)
        Payment.belongsTo(models.PaymentMethod)
    }

    return Payment
}
