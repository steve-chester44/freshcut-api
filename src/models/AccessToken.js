export default (sequelize, DataTypes) => {
    const Token = sequelize.define("access_token", {
        valid: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        key: {
            type: DataTypes.STRING,
            allowNull: false
        }
    })

    Token.associate = models => {
        Token.belongsTo(models.Location, { onDelete: "CASCADE"})

    }

    return Token
}
