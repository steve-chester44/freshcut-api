export default (sequelize, DataTypes) => {
	const SaleItem = sequelize.define("sale_items", {
		name: DataTypes.STRING,
		description: DataTypes.STRING,
		price: DataTypes.FLOAT
	})

	SaleItem.associate = models => {
		SaleItem.belongsTo(models.Sale)
	}

	return SaleItem
}
