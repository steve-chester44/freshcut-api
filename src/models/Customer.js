import { validateNumber, sendText } from "../services/twilio"

export default (sequelize, DataTypes) => {
	const Customer = sequelize.define("customer", {
		firstName: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: "",
			validate: { notEmpty: { msg: "A first name is required for a customer." } }
		},
		lastName: DataTypes.STRING,
		contactNumber: DataTypes.STRING,
		contactEmail: { type: DataTypes.STRING },
		gender: { type: DataTypes.ENUM("male", "female", "unknown"), defaultValue: "unknown" },
		notes: DataTypes.STRING,
		acceptsMarketing: DataTypes.BOOLEAN,
		appointmentNotifications: {
			type: DataTypes.ENUM("sms", "email", "both", "none"),
			defaultValue: "none"
		},
		totalBookings: { type: DataTypes.INTEGER, defaultValue: 0 }
	})

	Customer.prototype.notifyBySMS = async function(message) {
		const canNotify =
			(this.appointmentNotifications === "sms" || this.appointmentNotifications === "both") &&
			String(this.contactNumber).length === 10

		if (!canNotify) return false

		validateNumber(`+1${this.contactNumber}`).then(() => {
			sendText(this.contactNumber, message)
		})
	}

	Customer.associate = models => {
		Customer.belongsTo(models.Company)
	}

	return Customer
}
