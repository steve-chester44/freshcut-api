import Sequelize from "sequelize"
import config from "../config"

const sequelize = new Sequelize(config.database.name, config.database.user, config.database.pass, {
	host: config.database.host,
	dialect: "mysql",
	operatorsAliases: Sequelize.Op,
	port: 3306,
	logging: false
})

const models = {
	User: sequelize.import("./User"),
	Company: sequelize.import("./Company"),
	Location: sequelize.import("./Location"),
	Employee: sequelize.import("./Employee"),
	Customer: sequelize.import("./Customer"),
	Appointment: sequelize.import("./Appointment"),
	AccessToken: sequelize.import("./AccessToken"),
	Service: sequelize.import("./Service"),
	ServiceGroup: sequelize.import("./ServiceGroup"),
	EmployeeService: sequelize.import("./EmployeeService"),
	PaymentMethod: sequelize.import("./PaymentMethod"),
	Payment: sequelize.import("./Payment"),
	Sale: sequelize.import("./Sale"),
	SaleItem: sequelize.import("./SaleItem"),
	BlockedTime: sequelize.import("./BlockedTime"),
	Subscription: sequelize.import("./Subscription"),
	Plan: sequelize.import("./Plan")
}

Object.keys(models).forEach(modelName => {
	if ("associate" in models[modelName]) {
		models[modelName].associate(models)
	}
})

models.sequelize = sequelize
models.Sequelize = Sequelize

export default models
