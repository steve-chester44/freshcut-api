export default (sequelize, DataTypes) => {
	const BlockedTime = sequelize.define("BlockedTime", {
		description: DataTypes.STRING,
		endTime: {
			type: DataTypes.DATE,
			allowNull: false,
			validate: {
				notEmpty: {
					msg: "An end time is required for blocked times."
				}
			}
		},
		startTime: {
			type: DataTypes.DATE,
			allowNull: false,
			validate: {
				notEmpty: {
					msg: "A start time is required for blocked times."
				}
			}
		}
	})

	BlockedTime.associate = models => {
		BlockedTime.belongsTo(models.Location)
		BlockedTime.belongsTo(models.User, { foreignKey: "employeeId" })
	}

	return BlockedTime
}
