export default (sequelize, DataTypes) => {
	const Sale = sequelize.define("sale", {
		total: DataTypes.FLOAT,
		totalReceived: DataTypes.FLOAT,
		paymentStatus: DataTypes.ENUM(["unpaid", "partial", "paid"])
	})

	Sale.associate = models => {
		Sale.belongsTo(models.Appointment)
		Sale.hasMany(models.Payment)
		Sale.hasMany(models.SaleItem)
	}

	return Sale
}
