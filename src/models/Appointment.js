export default (sequelize, DataTypes) => {
	const Appointment = sequelize.define(
		"appointment",
		{
			checkInTime: DataTypes.DATE,
			checkOutTime: DataTypes.DATE, // TODO: Set the checkOutTime when a status is set to completed/noshow
			paymentStatus: {
				type: DataTypes.ENUM(["unpaid", "partial", "paid"]),
				defaultValue: "unpaid"
			},
			duration: {
				type: DataTypes.FLOAT,
				defaultValue: 0
			},
			price: {
				type: DataTypes.FLOAT,
				defaultValue: 0
			},
			status: {
				type: DataTypes.ENUM(["unconfirmed", "confirmed", "in-progress", "completed", "canceled", "noshow"]),
				defaultValue: "confirmed"
			},
			endTime: {
				type: DataTypes.DATE,
				allowNull: false,
				validate: {
					notEmpty: {
						msg: "An end time is required for appointments."
					}
				}
			},
			startTime: {
				type: DataTypes.DATE,
				allowNull: false,
				validate: {
					notEmpty: {
						msg: "A start time is required for appointments."
					}
				}
			}
		}
	)

	Appointment.associate = models => {
		Appointment.belongsTo(models.Location)
		Appointment.belongsTo(models.User)
		Appointment.belongsTo(models.Customer)

		Appointment.belongsToMany(models.Service, { through: "appointment_services" })
		Appointment.hasMany(models.Sale)
	}

	return Appointment
}
