import bcrypt from "bcrypt"

export default (sequelize, DataTypes) => {
	const User = sequelize.define(
		"user",
		{
			email: {
				type: DataTypes.STRING,
				unique: { msg: "The email is already taken", fields: ["email"] },
				allowNull: true
			},
			password: {
				type: DataTypes.STRING,
				validate: { len: { args: [4, 100], msg: "The password must be at least 4 characters long" } }
			},
			firstName: { type: DataTypes.STRING, allowNull: false, trim: true },
			lastName: DataTypes.STRING,
			permissionLevel: {
				type: DataTypes.ENUM(["none", "limited", "regular", "full", "owner"]),
				defaultValue: "none"
			},
			bookingEnabled: { type: DataTypes.BOOLEAN, defaultValue: true },
			resetPasswordToken: DataTypes.STRING,
			count: {
				type: DataTypes.INTEGER,
				defaultValue: 0
			}
		},
		{
			hooks: {
				afterValidate: async user => {
					if (user.changed("password")) {
						user.password = await bcrypt.hash(user.password, 12)
					}
				}
			}
		}
	)

	User.associate = models => {
		User.belongsTo(models.Company)
		User.belongsToMany(models.Location, { through: models.Employee })
		User.belongsToMany(models.Service, { through: models.EmployeeService })

		User.hasMany(models.BlockedTime, { foreignKey: "employeeId" })
	}

	return User
}
