import baseResolver from "./baseResolver"
import { AuthenticationRequiredError, ForbiddenError } from "../utils/errors"
import { isAPIKeyRequest } from "../utils"

// A request that doesn't have a user in the context with at least a companyId should not be considered a valid request because theres not enough information to validate the request or authenticate the requester.
export const isValidRequestResolver = baseResolver.createResolver((root, input, ctx) => {
	if (!ctx.user || !ctx.user.companyId) throw new AuthenticationRequiredError()
})

export const isAccountOwnerResolver = isValidRequestResolver.createResolver(async (root, input, ctx) => {
	if (!ctx.user || !ctx.user.companyId) throw new ForbiddenError()

	const user = await ctx.models.User.findByPk(ctx.user.id)
	if (user.permissionLevel !== "owner") throw new ForbiddenError()
})

export const isAuthenticatedResolver = isValidRequestResolver.createResolver(async (root, input, ctx) => {
	if (isAPIKeyRequest(ctx.user)) {
		const token = await ctx.models.AccessToken.findOne({
			where: {
				key: ctx.user.key,
				locationId: ctx.user.locationId
			}
		})

		// return is the same as continue/next.. we have the token so move on to the next step.
		if (token) return
	}

	const user = await ctx.models.User.findOne({ where: { id: ctx.user.id, companyId: ctx.user.companyId } })

	if (!user) {
		throw new ForbiddenError()
	}
})
