import { createResolver } from "apollo-resolvers"
import { createError, isInstance } from "apollo-errors"
import { UnknownError } from "../utils/errors"

export default createResolver(
	//incoming requests will pass through this resolver like a no-op
	null,
	/*
    Only mask outgoing errors that aren't already apollo-errors,
    such as ORM errors etc
  */
	(root, args, context, error) => {
		console.log(error);
		return isInstance(error) ? error : new UnknownError({ internalData: { error } })
	}
)
