import jwt from "jsonwebtoken"
import config from "../config"

export const createAccessToken = (payload = {}, options = {}) => jwt.sign(payload, config.secrets.accessToken, options)

const createTokens = user => {
	return {
		accessToken: createAccessToken(
			{
				id: user.id,
				companyId: user.companyId,
				permissionLevel: user.permissionLevel
			},
			{ expiresIn: "60m" }
		),
		refreshToken: jwt.sign(
			{
				id: user.id,
				count: user.count
			},
			config.secrets.refreshToken,
			{ expiresIn: "7d" }
		)
	}
}

export default createTokens

export const createAndSetTokens = (user, res) => {
	const options = {}

	if (process.env.NODE_ENV === "production") {
		options.domain = "neverwait.app"
		options.secure = true
	}

	const { accessToken, refreshToken } = createTokens(user)

	res.cookie("access-token", accessToken, options)
	res.cookie("refresh-token", refreshToken, options)

	return { accessToken, refreshToken }
}
