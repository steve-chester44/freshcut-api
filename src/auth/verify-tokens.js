import jwt from "jsonwebtoken"
import config from "../config"
import models from "../models"
import { createAndSetTokens } from "./create-tokens"

const getToken = (req, name) => {
	const headerToken = req.headers[`x-${name}`]
	const cookieToken = req.cookies[name]

	if (cookieToken) return cookieToken

	if (headerToken && headerToken !== null && headerToken !== "null") {
		return headerToken
	}

	return null
}

const verifyTokens = async (req, res, next) => {
	const accessToken = getToken(req, "access-token")
	const refreshToken = getToken(req, "refresh-token")

	if (!accessToken && !refreshToken) {
		return next()
	}

	// has an access token
	if (accessToken) {
		try {
			// token is valid, proceed
			req.user = jwt.verify(accessToken, config.secrets.accessToken)
			return next()
		} catch (e) {}
	}

	let data

	// has refresh token
	if (refreshToken) {
		try {
			data = jwt.verify(refreshToken, config.secrets.refreshToken)
		} catch (e) {
			// token isn't good, so keep going (proceed to error)
			return next()
		}
	}

	// figure out if the refresh token is still valid (hasnt been invalidated)
	const user = await models.User.findByPk(data.id, { raw: true })
	// token has been invalidated
	if (!user || user.count !== data.count) {
		return next()
	}

	// If we made it this far then the user's access token has expired but his refresh token was still good.
	// creates new access & refresh tokens and returns them to the user.
	createAndSetTokens(user, res)
	next()
}

export default verifyTokens
