import findAllAppointments from "../../controllers/appointments/find-all"

import { isAuthenticatedResolver } from "../../auth/permissions"
import { NotFoundError, ForbiddenError } from "../../utils/errors"
import { getInputLocation } from "../../utils"
import { pubsub } from "../"

import { withFilter } from "graphql-subscriptions"
import { subscriptions } from "../constants"

import upsertAppointment from "../../controllers/appointments/upsert-appointment"
import deleteAppointment from "../../controllers/appointments/delete-appointment"

export default {
	Appointment: {
		employee: ({ userId }, args, ctx) => ctx.models.User.findByPk(userId),
		location: ({ locationId }, args, ctx) => ctx.models.Location.findByPk(locationId),
		customer: ({ customerId }, args, ctx) => ctx.models.Customer.findByPk(customerId),
		sales: ({ id }, args, ctx) => ctx.models.Sale.findAll({ raw: true, where: { appointmentId: id } }),
		services: async ({ id }, args, ctx) =>
			ctx.models.Service.findAll({
				raw: true,
				include: [
					{
						model: ctx.models.Appointment,
						where: { id }
					}
				]
			})
	},
	Query: {
		appointment: isAuthenticatedResolver.createResolver(async (root, { id }, ctx) => {
			const appointment = await ctx.models.Appointment.findByPk(id)

			if (!appointment) throw new NotFoundError()

			const location = await ctx.models.Location.findOne({
				where: {
					id: appointment.locationId,
					companyId: ctx.user.companyId
				}
			})

			if (!location) throw new ForbiddenError()

			return appointment
		}),
		appointments: isAuthenticatedResolver.createResolver((root, input, ctx) => {
			const locationId = getInputLocation(ctx.user, input)
			return findAllAppointments(ctx, { where: { locationId } }, input.input)
		})
	},
	Subscription: {
		AppointmentsChange: {
			subscribe: withFilter(
				() => pubsub.asyncIterator([subscriptions.APPOINTMENT_CREATED, subscriptions.APPOINTMENT_UPDATED]),
				(payload, variables) => {
					// TODO: Its possible for the user to pass any locationId here. locationId should either come from the token and/or we should be checking the ctx.user against the supplied locationId
					return +variables.locationId === +payload.AppointmentsChange.appointment.locationId
				}
			)
		}
	},
	Mutation: {
		upsertAppointment,
		deleteAppointment
	}
}
