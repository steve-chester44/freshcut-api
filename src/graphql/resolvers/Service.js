import { isAuthenticatedResolver } from "../../auth/permissions"
import { NotFoundError } from "../../utils/errors"
import { okErrorResponse, okSuccessResponse } from "../../utils"

export default {
	Service: {
		employees: isAuthenticatedResolver.createResolver(async ({ id }, input, ctx) => {
			return await ctx.models.User.findAll({
				include: [
					{
						model: ctx.models.Service,
						where: { id }
					}
				]
			})
		})
	},
	Query: {
		services: isAuthenticatedResolver.createResolver((root, input, ctx) => {
			return ctx.models.Service.findAll({ where: { companyId: ctx.user.companyId } })
		})
	},
	Mutation: {
		upsertService: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			try {
				const group = await ctx.models.ServiceGroup.findByPk(input.serviceGroupId)

				if (!group) {
					throw new NotFoundError()
				}

				let service
				let created = false
				let data = { ...input, companyId: ctx.user.companyId }

				if (input.id) {
					service = await ctx.models.Service.findOne({
						where: { id: input.id, companyId: ctx.user.companyId },
						include: [ctx.models.User]
					})

					if (!service) throw new NotFoundError()

					await service.update(data)
					await service.setUsers(input.employees.length > 0 ? input.employees : null)
				} else {
					service = await group.createService(data)
					created = true
				}

				return okSuccessResponse({ service, created })
			} catch (error) {
				console.log(error)
				return okErrorResponse({ errors: error.errors })
			}
		}),
		deleteService: isAuthenticatedResolver.createResolver(async (root, { id }, ctx) => {
			try {
				return await ctx.models.Service.destroy({ where: { id } })
			} catch (error) {
				return false
			}
		})
	}
}
