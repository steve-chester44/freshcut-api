import { isAuthenticatedResolver } from "../../auth/permissions"
import { okErrorResponse, okSuccessResponse } from "../../utils"
import moment from "moment"
import { NotFoundError } from "../../utils/errors"

export default {
	Customer: {
		appointments: isAuthenticatedResolver.createResolver(({ id }, { past, future }, ctx) => {
			const now = moment()

			return {
				past: ctx.models.Appointment.findAll({
					limit: 10,
					where: {
						customerId: id,
						startTime: {
							[ctx.models.Sequelize.Op.lte]: now.toDate()
						}
					}
				}),
				upcoming: ctx.models.Appointment.findAll({
					limit: 2,
					where: {
						customerId: id,
						startTime: {
							[ctx.models.Sequelize.Op.gte]: now.toDate()
						}
					}
				})
			}
		})
	},
	Query: {
		customers: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			return ctx.models.Customer.findAll({
				offset: input.offset || 0,
				limit: input.first || 250,
				order: [["firstName", "ASC"]],
				where: {
					companyId: ctx.user.companyId
				}
			})
		}),
		customer: isAuthenticatedResolver.createResolver(async (root, { id }, ctx) => {
			return await ctx.models.Customer.findByPk(id)
		}),
		searchCustomers: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			return await ctx.models.Customer.findAll({
				order: [["firstName", "ASC"]],
				where: {
					companyId: ctx.user.companyId,
					[ctx.models.Sequelize.Op.or]: [
						ctx.models.Sequelize.where(
							ctx.models.Sequelize.fn(
								"concat",
								ctx.models.Sequelize.col("firstName"),
								" ",
								ctx.models.Sequelize.col("lastName")
							),
							{
								[ctx.models.Sequelize.Op.like]: `%${input.term}%`
							}
						),
						{
							contactNumber: {
								[ctx.models.Sequelize.Op.like]: `%${input.term}%`
							}
						}
					]
				}
			})
		})
	},
	Mutation: {
		UpdateCustomer: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			try {
				const customer = await ctx.models.Customer.findOne({
					where: {
						id: input.id,
						companyId: ctx.user.companyId
					}
				})

				if (!customer) {
					throw NotFoundError()
				}

				await customer.update(input)

				return okSuccessResponse({ customer })
			} catch (error) {
				console.log(error)
				return okErrorResponse({ errors: error.errors })
			}
		}),
		CreateCustomer: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			try {
				const customer = await ctx.models.Customer.create({ ...input, companyId: ctx.user.companyId })
				return okSuccessResponse({ customer })
			} catch (error) {
				console.log(error)
				return okErrorResponse({ errors: error.errors })
			}
		}),
		findOrCreateCustomer: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			try {
				const [customer, created] = await ctx.models.Customer.findOrCreate({
					where: {
						contactNumber: input.contactNumber,
						companyId: ctx.user.companyId
					},
					defaults: { ...input }
				})

				return okSuccessResponse({ customer })
			} catch (error) {
				return okErrorResponse({ errors: error.errors })
			}
		})
	}
}
