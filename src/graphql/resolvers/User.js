import register from "../../controllers/auth/register"
import login from "../../controllers/auth/login"
import setAccountPassword from "../../controllers/auth/set-account-password"
import upsertEmployee from "../../controllers/user/upsert-user"

import { okSuccessResponse, okErrorResponse } from "../../utils"
import { isAuthenticatedResolver } from "../../auth/permissions"

import findAllAppointments from "../../controllers/appointments/find-all"
import findAllBlockedTimes from "../../controllers/appointments/blocked-time"
import { sendEmail, ACCOUNT_DELETED } from "../../utils/email"

export default {
	User: {
		company: async (root, input, ctx) => ctx.models.Company.findByPk(ctx.user.companyId),
		locations: async ({ id }, input, ctx) => {
			return ctx.models.Location.findAll({
				include: [
					{
						model: ctx.models.User,
						where: { id }
					}
				]
			})
		},
		services: async ({ id }, input, ctx) => {
			return ctx.models.Service.findAll({
				include: [{ model: ctx.models.User, where: { id } }]
			})
		},
		blockedTimes: ({ id }, { input }, ctx) => findAllBlockedTimes(ctx, { where: { employeeId: id } }, input),
		appointments: ({ id }, { input }, ctx) => findAllAppointments(ctx, { where: { userId: id } }, input)
	},
	Query: {
		employees: isAuthenticatedResolver.createResolver((root, input, ctx) =>
			ctx.models.User.findAll({
				where: {
					companyId: ctx.user.companyId
				}
			})
		),
		me: isAuthenticatedResolver.createResolver((root, input, ctx) => ctx.models.User.findByPk(ctx.user.id))
	},
	Mutation: {
		upsertEmployee: (root, input, ctx) => upsertEmployee(input, ctx),
		deleteEmployee: async (root, { id }, ctx) => {
			try {
				const user = await ctx.models.User.findByPk(id, { raw: true })
				const deleted = await ctx.models.User.destroy({ where: { id, companyId: ctx.user.companyId } })

				if (deleted) {
					sendEmail(user.email, ACCOUNT_DELETED)
				}

				return deleted
			} catch (e) {
				return false
			}
		},

		updateMyProfile: async (root, { input }, ctx) => {
			try {
				await ctx.models.User.update(input, {
					where: {
						id: ctx.user.id
					}
				})

				return okSuccessResponse()
			} catch ({ errors }) {
				return okErrorResponse({ errors })
			}
		},
		setAccountPassword: async (root, input, ctx) => setAccountPassword(input, ctx),
		login: async (root, { email, password }, ctx) => login(email, password, ctx.models, ctx.res),

		//TODO: logout doesn't work
		logout: async (root, args, ctx) => ctx.res.clearCookie("token"),
		register: async (root, args, ctx) => register(args, ctx.models)
	}
}
