import hat from "hat"
import { createAccessToken } from "../../auth/create-tokens"
import { isAuthenticatedResolver } from "../../auth/permissions"
import { okSuccessResponse, okErrorResponse } from "../../utils"

export const CreateToken = isAuthenticatedResolver.createResolver(async (root, input, ctx) => {
	try {
		const key = hat()

		const token = await ctx.models.AccessToken.create({
			key: `${ctx.user.companyId}${key}`,
			locationId: input.locationId
		})

		return okSuccessResponse({ token })
	} catch (error) {
		return okErrorResponse({ errors: error.errors })
	}
})

const AuthWithToken = async (root, input, ctx) => {
	try {
		const existing = await ctx.models.AccessToken.findOne({
			where: { key: input.key },
			include: [ctx.models.Location]
		})

		if (!existing || !existing.valid) {
			return okErrorResponse({ errors: [{ path: "auth", message: "Invalid Key" }] })
		}

		const token = createAccessToken({
			// isAPIKey tells the server that this access token isn't a normal User-based access token. This token doesn't have a userId so we the server needs to find data and verify the request differently.
			isAPIKey: true,
			locationId: existing.location.id,
			companyId: existing.location.companyId,
			// key is needed to verify the key belongs to the location sent along with the request
			key: input.key
		})

		return okSuccessResponse({ token })
	} catch (error) {
		console.log(error)
		return okErrorResponse({ errors: error.errors })
	}
}

export default {
	Mutation: {
		AuthWithToken,
		CreateToken
	}
}
