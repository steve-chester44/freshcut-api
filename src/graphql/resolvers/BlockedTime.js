import { withFilter } from "graphql-subscriptions"

import { isAuthenticatedResolver } from "../../auth/permissions"
import { okErrorResponse, okSuccessResponse } from "../../utils"
import { NotFoundError, NotYourEntityError, UnknownError } from "../../utils/errors"

import { pubsub } from "../"
import { getInputLocation } from "../../utils"
import { subscriptions } from "../constants"

export default {
	Query: {
		BlockedTimes: isAuthenticatedResolver.createResolver(async (root, { locationId, employeeId }, ctx) => {
			if (!locationId) throw new ForbiddenError()
			const location = await ctx.models.Location.findByPk(locationId)
			if (!location || location.companyId !== ctx.user.companyId) throw new NotYourEntityError()

			const queryData = { locationId }

			if (employeeId) {
				queryData.employeeId = employeeId
			}

			return ctx.models.BlockedTime.findAll({ where: queryData })
		})
	},
	Mutation: {
		upsertBlockedTime: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			input.locationId = getInputLocation(ctx.user, input)

			try {
				if (!input.locationId) {
					throw new ForbiddenError()
				}

				const upsert = async () => {
					let blockedTime

					if (input.id) {
						const entity = await ctx.models.BlockedTime.findByPk(input.id)
						if (!entity) throw new NotFoundError()

						blockedTime = await entity.update(input)

						return { blockedTime: { ...blockedTime.dataValues, ...input }, created: false }
					} else {
						blockedTime = await ctx.models.BlockedTime.create(input)

						if (!blockedTime) return UnknownError()

						return { blockedTime: blockedTime.dataValues, created: true }
					}
				}

				const response = await upsert()

				pubsub.publish(response.isNewRecord ? subscriptions.BLOCKED_TIME_CREATED : subscriptions.BLOCKED_TIME_UPDATED, {
					BlockedTimeChange: {
						blockedTime: response.blockedTime,
						employeeId: response.blockedTime.employeeId
					}
				})

				return okSuccessResponse({
					blockedTime: response.blockedTime,
					locationId: input.locationId
				})
			} catch (error) {
				console.log(error)
				return okErrorResponse({ errors: [error] })
			}
		}),
		deleteBlockedTime: isAuthenticatedResolver.createResolver(async (root, { id }, ctx) => {
			try {
				const time = await ctx.models.BlockedTime.findByPk(id)

				if (!time) throw new NotFoundError()

				const location = await ctx.models.Location.findByPk(time.locationId)

				if (!location || location.companyId !== ctx.user.companyId) {
					throw new NotYourEntityError()
				}

				pubsub.publish(subscriptions.BLOCKED_TIME_UPDATED, {
					BlockedTimeChange: {
						deleted: true,
						employeeId: time.dataValues.employeeId,
						blockedTime: time.dataValues
					}
				})

				return time.destroy()
			} catch (error) {
				console.log(error)
				return false
			}
		})
	},
	Subscription: {
		BlockedTimeChange: {
			subscribe: withFilter(
				() => pubsub.asyncIterator([subscriptions.BLOCKED_TIME_CREATED, subscriptions.BLOCKED_TIME_UPDATED]),
				(payload, variables) => {
					// TODO: Its possible for the user to pass any locationId here. locationId should either come from the token and/or we should be checking the ctx.user against the supplied locationId
					return +variables.locationId === +payload.BlockedTimeChange.blockedTime.locationId
				}
			)
		}
	}
}
