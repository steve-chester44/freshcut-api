import { isAccountOwnerResolver } from "../../auth/permissions"
import { stripe } from "../../services/stripe"


// TODO:: -- These routes cant be correct... its possible to cancel your sub & re-sub (I THINK.)
// These need to be looked over and corrected since they're financially critical.
export default {
	Mutation: {
		cancelSubscription: isAccountOwnerResolver.createResolver(async (root, { input }, ctx) => {
			const freePlan = await ctx.models.Plan.findOne({ where: { accessLevel: "free" } })
			const company = await ctx.models.Company.findByPk(ctx.user.companyId)

			await stripe.customers.update(company.stripeCustomerId, { plan: freePlan.stripeId })
			await ctx.models.Subscription.update({ planId: freePlan.id }, { where: { companyId: company.id } })

			return ctx.models.Company.findByPk(ctx.user.companyId)
		}),

		// TODO: This is an internal route only. should never be exposed to the public... how to?
		// TODO: Restrict this route to users with correct permissions instead of only the account owner
		changeSubscription: isAccountOwnerResolver.createResolver(async (root, { input }, ctx) => {
			// new plan
			const plan = await ctx.models.Plan.findOne({ where: { stripeId: input.plan } }, { raw: true })
			const company = await ctx.models.Company.findByPk(ctx.user.companyId)

			if (!plan) {
				console.log(input);
				// TODO: throw better error
				throw Error("Plan does not exist")
			}

			const details = {
				plan: input.plan,
				...(input.source ? { source: input.source } : {})
			}

			await stripe.customers.update(company.stripeCustomerId, details)

			await ctx.models.Subscription.update(
				{ planId: plan.id },
				{
					where: {
						companyId: company.id
					}
				}
			)

			return ctx.models.Company.findByPk(ctx.user.companyId)
		})
	}
}
