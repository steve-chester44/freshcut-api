import { okErrorResponse, okSuccessResponse, getInputLocation } from "../../utils"
import { isAuthenticatedResolver } from "../../auth/permissions"

import findAllAppointments from "../../controllers/appointments/find-all"
import findBlockedTime from "../../controllers/appointments/blocked-time"

export default {
	Query: {
		locations: isAuthenticatedResolver.createResolver((root, input, ctx) =>
			ctx.models.Location.findAll({ where: { companyId: ctx.user.companyId } })
		),
		location: isAuthenticatedResolver.createResolver((root, input, ctx) => {
			const locationId = getInputLocation(ctx.user, input)
			if (!locationId) return null

			return ctx.models.Location.findByPk(locationId)
		})
	},
	Mutation: {
		CreateLocation: isAuthenticatedResolver.createResolver(async (root, { input }, ctx) => {
			return await ctx.models.Company.findByPk(ctx.user.companyId)
				.then(company => {
					const location = company.createLocation({ name: input.name })

					return okSuccessResponse({ location })
				})
				.catch(error => okErrorResponse({ errors: error.errors }))
		})
	},
	Location: {
		company: async ({ companyId }, input, ctx) => ctx.models.Company.findByPk(companyId),
		serviceGroups: ({ companyId }, input, ctx) => ctx.models.ServiceGroup.findAll({ where: { companyId } }),

		employees: ({ id }, { input: { where: { bookingEnabled } = {} } = {} }, ctx) => {
			const where = {}

			if (bookingEnabled === false || bookingEnabled === true) {
				where.bookingEnabled = bookingEnabled
			}

			return ctx.models.User.findAll({
				where,
				raw: true,
				include: [
					{
						model: ctx.models.Location,
						where: { id }
					}
				]
			})
		},
		blockedTime: ({ id }, { input }, ctx) => findBlockedTime(ctx, { where: { locationId: id } }, input),
		appointments: ({ id }, { input }, ctx) => findAllAppointments(ctx, { where: { locationId: id } }, input)
	}
}
