import { isAuthenticatedResolver } from "../../auth/permissions"

const limits = {
	free: {
		accounts: 1,
		checkin: false,
		waitwidget: false
	},
	low: {
		accounts: 3,
		checkin: false,
		waitwidget: true
	},
	medium: {
		accounts: 10,
		checkin: true,
		waitwidget: true
	},
	high: {
		accounts: 999,
		checkin: true,
		waitwidget: true
	}
}

export default {
	Company: {
		locations: ({ id }, args, ctx) => ctx.models.Location.findAll({ where: { companyId: id } }),
		employees: ({ id }, args, ctx) => ctx.models.User.findAll({ where: { companyId: id } }),
		subscription: async ({ id }, args, ctx) => {
			const plan = await ctx.models.Plan.findOne({
				include: [
					{
						model: ctx.models.Company,
						where: { id }
					}
				],
				raw: true
			})

			if (!plan) {
				return null
			}

			// TODO: Not getting latest subscription

			const subscription = await ctx.models.Subscription.findOne({
				order: [["createdAt", "DESC"]],
				where: {
					companyId: id,
					planId: plan.id
				},
				raw: true
			})

			return {
				...subscription,
				plan,
				limitations: limits[plan.accessLevel]
			}
		}
	},
	Query: {
		company: isAuthenticatedResolver.createResolver((root, input, ctx) =>
			ctx.models.Company.findByPk(ctx.user.companyId)
		)
	}
}
