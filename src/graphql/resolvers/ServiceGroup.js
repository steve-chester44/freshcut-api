import { isAuthenticatedResolver } from "../../auth/permissions"
import { okErrorResponse, okSuccessResponse } from "../../utils"
import { MissingInputError, NotFoundError } from "../../utils/errors"

export default {
	ServiceGroup: {
		services: ({ id }, input, ctx) => ctx.models.Service.findAll({ where: { serviceGroupId: id } })
	},
	Query: {
		serviceGroups: isAuthenticatedResolver.createResolver((root, input, ctx) =>
			ctx.models.ServiceGroup.findAll({ where: { companyId: ctx.user.companyId } })
		)
	},
	Mutation: {
		deleteServiceGroup: isAuthenticatedResolver.createResolver(async (root, { id }, ctx) =>
			ctx.models.ServiceGroup.destroy({ where: { id, companyId: ctx.user.companyId } })
		),
		// TODO: should be renamed to upsertServiceGroup
		// Sequelize's `upsert` method doesn't return an ID so we must upsert manually
		updateOrCreateServiceGroup: isAuthenticatedResolver.createResolver(
			async (root, { input: { id, name } }, ctx) => {
				let group

				if (name.length === 0) throw new MissingInputError({ message: `Name field cannot be empty` })

				try {
					if (id) {
						group = await ctx.models.ServiceGroup.findByPk(id)

						if (!group) throw new NotFoundError()

						group = await group.update({ name })
						return okSuccessResponse({ group })
					} else {
						group = await ctx.models.ServiceGroup.create({ name, companyId: ctx.user.companyId })
					}

					return okSuccessResponse({ group })
				} catch (error) {
					return okErrorResponse({ errors: error.errors })
				}
			}
		)
	}
}
