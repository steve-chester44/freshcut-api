import { isAuthenticatedResolver } from "../../auth/permissions"

export default {
	Query: {
		paymentMethods: isAuthenticatedResolver.createResolver((root, input, ctx) => {
            // TODO - paymentMethods should be tied to a locationId
            return ctx.models.PaymentMethod.findAll()
		})
	}
}
