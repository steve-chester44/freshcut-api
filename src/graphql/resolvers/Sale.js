import { isAuthenticatedResolver } from "../../auth/permissions"
import { NotFoundError } from "../../utils/errors"
import { okErrorResponse, okSuccessResponse } from "../../utils"
import { format } from "date-fns"

import { pubsub } from "../"
import { subscriptions } from "../constants"

export default {
	Sale: {
		appointment: ({ appointmentId }, input, ctx) => {
			return ctx.models.Appointment.findByPk(appointmentId)
		},
		payments: async ({ id }, input, ctx) => {
			return ctx.models.Payment.findAll({ where: { saleId: id } })
		},
		items: async ({ id }, input, ctx) => {
			return ctx.models.SaleItem.findAll({ where: { saleId: id } })
		}
	},
	Query: {
		sale: isAuthenticatedResolver.createResolver((root, { appointmentId }, ctx) => {
			return ctx.models.Sale.findOne({ where: { appointmentId } })
		})
	},
	Mutation: {
		Checkout: isAuthenticatedResolver.createResolver(
			async (root, { input: { paymentAmount, paymentMethodId, appointmentId } }, ctx) => {
				const appointment = await ctx.models.Appointment.findByPk(appointmentId)

				if (!appointment) throw new NotFoundError()

				try {
					const sale = await ctx.models.sequelize.transaction(async transaction => {
						const paymentStatus =
							paymentAmount >= appointment.price ? "paid" : paymentAmount > 0 ? "partial" : "unpaid"

						const services = await appointment.getServices({ raw: true })
						const employee = await appointment.getUser({ raw: true })

						const sale = await ctx.models.Sale.create(
							{
								total: appointment.price,
								totalReceived: paymentAmount,
								paymentStatus,
								appointmentId: appointment.id
							},
							{ transaction }
						)

						const saleItemsData = services.map(service => {
							return {
								saleId: sale.dataValues.id,
								name: service.name,
								price: service.price,
								description: `${format(appointment.startTime, "h:mma MMM Do, YYYY")} with ${
									employee.firstName
								} ${employee.lastName ? employee.lastName : ""}`
							}
						})

						if (saleItemsData.length > 0) {
							await ctx.models.SaleItem.bulkCreate(saleItemsData, { transaction })
						}

						await ctx.models.Payment.create(
							{
								amount: paymentAmount,
								status: "completed",
								saleId: sale.dataValues.id,
								paymentMethodId
							},
							{ transaction }
						)

						if (paymentStatus !== "unpaid") {
							await appointment.update({ paymentStatus, status: "completed" }, { transaction })

							pubsub.publish(subscriptions.APPOINTMENT_UPDATED, {
								AppointmentsChange: {
									employeeId: appointment.dataValues.userId,
									appointment: appointment.dataValues
								}
							})
						}

						return sale
					})

					return okSuccessResponse({ sale })
				} catch (error) {
					console.log(error)
					return okErrorResponse({ errors: error.errors })
				}
			}
		)
	}
}
