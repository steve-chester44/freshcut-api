import path from "path"
import chalk from "chalk"
import jwt from "jsonwebtoken"

import { execute, subscribe } from "graphql"
import { PubSub } from "graphql-subscriptions"
import { SubscriptionServer } from "subscriptions-transport-ws"
import { makeExecutableSchema } from "graphql-tools"
import { formatError } from "apollo-errors"
import { fileLoader, mergeTypes, mergeResolvers } from "merge-graphql-schemas"

import { ForbiddenError } from "../utils/errors"

import config from "../config"
import models from "../models"

export const pubsub = new PubSub()

const schema = makeExecutableSchema({
	typeDefs: mergeTypes(fileLoader(path.join(__dirname, "./schema"))),
	resolvers: mergeResolvers(fileLoader(path.join(__dirname, "./resolvers")))
})

export const createSubscriptionServer = ({ server, path }) => {
	return new SubscriptionServer(
		{
			execute,
			subscribe,
			schema,
			onConnect: async params => {
				if (params.token) {
					try {
						const user = jwt.verify(params.token, config.secrets.accessToken)
						return { models, user }
					} catch (error) {
						console.log("Subscription Error", error)
						throw new ForbiddenError(error)
					}
				}

				console.log("token used:", params.token)
				console.log(chalk.red("subscriber tried to connect with no token"))
				throw new ForbiddenError()
			}
		},
		{ server, path }
	)
}

export const graphqlExpressConfig = (req, res) => ({
	schema,
	formatError,
	context: {
		user: req.user,
		// user: req.user || {
		// 	id: 1,
		// 	companyId: 1,
		// 	permissionLevel: "owner"
		// },
		pubsub,
		models,
		res,
		req
	},
	pretty: process.env.NODE_ENV === "production"
})
