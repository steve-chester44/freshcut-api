require("dotenv").config()

module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.bulkInsert(
			"payment_methods",
			[
				{
					name: "Cash",
					createdAt: new Date(),
					updatedAt: new Date()
				},
				{
					name: "Card",
					createdAt: new Date(),
					updatedAt: new Date()
				}
			],
			{}
		)
	},

	down: function(queryInterface, Sequelize) {
		queryInterface.bulkDelete("payment_methods", [
			{
				[Sequelize.Op.or]: [
					{
						name: "Card"
					},
					{
						name: "Cash"
					}
				]
			}
		])
	}
}
