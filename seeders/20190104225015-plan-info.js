require("dotenv").config()

// STRIPE_PLAN_YEAR_LOW = "plan_DwfHHUzL4ZiCk6"
// STRIPE_PLAN_YEAR_MED = "plan_Dwg4za5R3ThvHE"
// STRIPE_PLAN_YEAR_HIGH = ""

const isProd = process.env.NODE_ENV === "production"

module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.bulkInsert(
			"plans",
			[
				{
					name: isProd ? "Free Plan" : "Dev - Free Plan",
					accessLevel: "free",
					stripeId: process.env.STRIPE_PLAN_FREE,
					pricePerMonth: "0",
					createdAt: new Date(),
					updatedAt: new Date()
				},
				{
					name: isProd ? "Basic Plan" : "Dev - Basic Plan (Monthly)",
					accessLevel: "low",
					stripeId: process.env.STRIPE_PLAN_MONTH_LOW,
					pricePerMonth: "29",
					createdAt: new Date(),
					updatedAt: new Date()
				},
				{
					name: isProd ? "Fresh Plan" : "Dev - Fresh Plan (Monthly)",
					accessLevel: "medium",
					stripeId: process.env.STRIPE_PLAN_MONTH_MED,
					pricePerMonth: "79",
					createdAt: new Date(),
					updatedAt: new Date()
				},
				{
					name: isProd ? "Super Fresh" : "Dev - Super Fresh (Monthly)",
					accessLevel: "high",
					stripeId: process.env.STRIPE_PLAN_MONTH_HIGH,
					pricePerMonth: "79",
					createdAt: new Date(),
					updatedAt: new Date()
				}
			],
			{}
		)
	},

	down: function(queryInterface, Sequelize) {
		queryInterface.bulkDelete("plans", [
			{
				accessLevel: "free"
			}
		])
	}
}
